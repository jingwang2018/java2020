package com.bibao.code;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DummyTest {
	private Dummy dummy;

	@Before
	public void setUp() throws Exception {
		dummy = new Dummy();
	}

	@Test
	public void testSayHello() {
		assertEquals("Hello World!", dummy.sayHello());
	}

}
